import json
import robin_stocks as rh
from pprint import pprint
from datetime import datetime,date,timedelta
from pandas_datareader import data as web
import pandas as pd
import time
import warnings
from math import floor
warnings.filterwarnings('ignore')


class Investor:
    def __init__(self,username,password,max_investment):
        self.username = username
        self.password = password
        self.max_investment = max_investment

    def method(self):
        pass


class Trade:
    STATUS_NEW = 'NEW'
    STATUS_POSTED = 'POSTED'
    STATUS_COMPLETE = 'COMPLETE'
    def __init__(self,ticker,limit,quantity):
        self.ticker = ticker
        self.limit = limit
        self.quantity = quantity
        self.status = self.STATUS_NEW
        self.id = ''

    def run(self):
        assert self.status != self.STATUS_COMPLETE, 'This trade has already been executed'
        self.execute()
        self.status = self.STATUS_POSTED

    def confirm_trade(self,id):
        raise NotImplementedError(f'{self.__class__.__name__} must implement confirm_trade')

    def update_status(self,id):
        if self.status == self.STATUS_COMPLETE:
            return None
        if self.confirm_trade(id):
            self.status = self.STATUS_COMPLETE
        return None

    def execute(self):
        raise NotImplementedError(f'{self.__class__.__name__} must implement execute')


class LimitBuy(Trade):
    def execute(self,bank_account=None):
        order = rh.order_buy_limit(symbol, quantity, limitPrice, timeInForce='gfd', extendedHours=False)
        time.sleep(2)
        self.id = order['id']


    def confirm_trade(self,id):
        info = rh.get_order_info(id)
        while info['state']!='confirmed':
            info = rh.get_order_info(id)
            time.sleep(2)
        return True


class LimitSell(Trade):
    def execute(self,bank_account):
        robin_stocks.orders.order_sell_limit(symbol, quantity, limitPrice, timeInForce='gfd', extendedHours=False)
        time.sleep(2)
        self.id = order['id']


    def confirm_trade(self,id):
        info = rh.get_order_info(id)
        while info['state']!='confirmed':
            info = rh.get_order_info(id)
            time.sleep(2)
        return True


class TestBuy(Trade):
    def execute(self,bank_account):
        bank_account -= self.limit*self.quantity
        print("Post-Buy Bank Account Balance: {}".format(bank_account))
        print("{} bought at {} - Quantity bought: {}".format(self.ticker,self.limit,self.quantity))
        return bank_account

    def confirm_trade(self,id=None):
        return True


class TestSell(Trade):
    def execute(self,bank_account):
        bank_account += self.limit*self.quantity
        print("Post-Sell Bank Account Balance: {}".format(bank_account))
        print("{} sold at {} - Quantity sold: {}".format(self.ticker,self.limit,self.quantity))
        return bank_account

    def confirm_trade(self,id=None):
        quote = float(rh.get_latest_price(self.ticker)[0])
        while quote<self.limit:
            time.sleep(2)
            quote = float(rh.get_latest_price(self.ticker)[0])
        return True


class Quote:
    def __init__(self,ticker,quote=None):
        self.ticker = ticker
        self.quote = quote
        self.trigger = None
        if self.quote is None:
            self.set_quote_from_ticker()

    def get_quote_from_ticker(self):
        return float(rh.get_latest_price(self.ticker)[0])

    def set_quote_from_ticker(self):
        self.quote = self.get_quote_from_ticker()
        return None

    def get_trigger(self):
        assert self.quote, f'No quote was found for {self.ticker}'
        return self.quote*0.998

    def set_trigger(self):
        self.trigger = self.get_trigger()

    def __str__(self):
        return f'{self.ticker}/{self.quote}'


class BollingerBand:
    @classmethod
    def from_ticker(cls,ticker,start,end):
        info = web.DataReader(ticker, data_source='yahoo', start=start, end=end)['Adj Close']
        data = pd.DataFrame(info)
        return cls.from_dataframe(ticker,data)

    @classmethod
    def from_dataframe(cls,ticker,data):
        data['30 Day MA'] = data['Adj Close'].rolling(window=20).mean()

        # set .std(ddof=0) for population std instead of sample
        data['30 Day STD'] = data['Adj Close'].rolling(window=20).std(ddof=0)
        #Temporarily changing to 1x so that I can actually see Buy/Sell
        condition = 1
        data['Upper Band'] = data['30 Day MA'] + (data['30 Day STD'] * condition)
        data['Lower Band'] = data['30 Day MA'] - (data['30 Day STD'] * condition)
        return cls(ticker,data['Lower Band'][-1],data['Upper Band'][-1])


    def __init__(self,ticker,lower_band,upper_band):
        self.ticker = ticker
        self.lower_band = lower_band
        self.upper_band = upper_band

    def __str__(self):
        return f'{self.ticker}/{self.lower_band}/{self.upper_band}'


class BollingerBandSet:
    def __init__(self,tickers,start,end):
        self.tickers = tickers
        self.start = start
        self.end = end
        self.bollinger_bands = {}

    def create_bands(self):
        for ticker in self.tickers:
            bb = BollingerBand.from_ticker(ticker,self.start,self.end)
            self.bollinger_bands[ticker]=bb

    def get_quotes(self):
        return [Quote(ticker) for ticker in self.tickers]

    def get_watch_quotes(self):
        assert self.bollinger_bands, "Bollinger Bands have not been instantiated"
        quotes = self.get_quotes()
        watch_quotes = []
        for quote in quotes:
            bb = self.bollinger_bands[quote.ticker]
            if quote.quote <= bb.lower_band:
                watch_quotes.append(quote)
        return watch_quotes


class RhCRACKED:
    buy_class = TestBuy
    def __init__(self):
        '''
        These three dictionaries will contain all collected daily data
        used to identify bull flags - and watch the price of the marked
        tickers after a flag is identified. Using this we can begin to draft
        a buy sell strategy
        '''
        self.tickers = ['AAPL','SPY','NFLX','MSFT','NVDA','APT','NKE']
        self.watch_quotes = {}
        self.last_month = ''
        self.today = ''
        self.bank_account = 5000.0

    def login(self):
        content = open('config.json').read()
        config = json.loads(content)
        rh.login(username=config['username'],password=config['password'])
        return None

    def watchlist(self):
        pass

    def stop_loss(self):
        pass
    #Returns previous month and todays date
    def get_months(self):
        self.today = datetime.strftime(date.today(), '%m/%d/%Y')
        self.last_month = datetime.strftime((date.today()-timedelta(days=30)), '%m/%d/%Y')
        return None

    def try_execute(self):
        print('Looking for Viable Trades')
        for quote in self.watch_quotes:
            quote.set_trigger()
            print(f'{quote.ticker} -trigger- {quote.trigger}')
        ### Rapid Decay Check
        for i in range(300):
            for quote in self.watch_quotes:
                if quote.quote < quote.trigger:
                    buy = self.buy_class(ticker=quote.ticker,limit=quote.quote,quantity=floor(1000/quote.quote))

                    self.bank_account = buy.execute(self.bank_account)
                    print('Executed Buy')
                    return buy

            time.sleep(1)
            for quote in self.watch_quotes:
                quote.set_quote_from_ticker()
        return None
    #change bollinger strategy to take multiple tickers
    def bollinger_strategy(self,tickers):
        self.login()
        self.get_months()
        bbs = BollingerBandSet(tickers,self.last_month,self.today)
        bbs.create_bands()
        self.watch_quotes = bbs.get_watch_quotes()

        for quote in self.watch_quotes:
            print(quote)
        # exit()

        buy = None
        while buy is None:
            buy = self.try_execute()

        while buy.status != buy.STATUS_COMPLETE:
            buy.update_status(buy.id)
            time.sleep(1)
            ### Add some confirmation
        ts = TestSell(buy.ticker,buy.limit*1.015,buy.quant)
        while ts.status != ts.STATUS_COMPLETE:
            ts.update_status(ts.id)
            time.sleep(2)
        if ts.status == ts.STATUS_COMPLETE:
            ts.execute(self.bank_account)

                ### Add in limit
                ###Wait for confirmation and break
        return None

    ###Need to make a correlation between trigger and current price to impact sell
    ###This will sell at a loss - but minimize the loss

if __name__=='__main__':
    print('\n')
    RH = RhCRACKED()
    RH.bollinger_strategy(RH.tickers)
    print('\n')
