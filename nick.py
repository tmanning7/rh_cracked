import json
import robin_stocks as rh
from datetime import datetime,date,timedelta
from pandas_datareader import data as web
import pandas as pd
import numpy as np
import time
import warnings
warnings.filterwarnings('ignore')
from robin_hood_cracked import Quote


WATCH_LIST_DATA = pd.read_excel('watch_list.xlsx',header=0)
WATCH_LIST = WATCH_LIST_DATA['Ticker'].to_list()

if __name__=='__main__':
    rh.login(username='',password='')

    for ticker in WATCH_LIST:
        try:
            now = datetime.now()
            time = now.strftime("%H:%M:%S")
            quote = float(rh.get_latest_price(ticker)[0])
            WATCH_LIST_DATA.loc[WATCH_LIST_DATA['Ticker']==ticker,'Quote'] = quote
            WATCH_LIST_DATA.loc[WATCH_LIST_DATA['Ticker']==ticker,'Time'] = time
            WATCH_LIST_DATA.loc[(WATCH_LIST_DATA['Quote']<WATCH_LIST_DATA['Price']),'FLAG'] = 'PRICE DECREASED'
            WATCH_LIST_DATA.loc[WATCH_LIST_DATA['FLAG'].isnull(),'FLAG'] = 'PRICE INCREASED'
        except:
            print(f'{ticker} not found')
            continue

    WATCH_LIST_DATA.to_excel(f'Modified List - {time}.xlsx',index=False)
